package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the first number");
        float num1 = input.nextInt();
        System.out.println("Enter the second number");
        float num2 = input.nextInt();
        if (num2 < 1) {
            do {
                System.out.println("Number entered is invalid. Please try again.");
                num2 = input.nextInt();
            } while (num2 < 1);
        }
        cal(num1, num2);
        input.close();
    }
    public static void cal(float num1, float num2) throws ArithmeticException{
        float answer = num1 / num2;
        System.out.println(num1 + " divided by " + num2 + " equals " + answer + ".");
        }
    }
