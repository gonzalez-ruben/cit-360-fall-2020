package JSONandHTTP;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Main {
    public static void main(String[] args) throws IOException {

        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/test", new ResponseHandler());
        server.setExecutor(null);
        server.start();

        Person json2 = JSON.JSONtoPerson(HTTP.getContent("http://localhost:8000/test"));
        System.out.println(json2);
        server.stop(3);
    }




}
