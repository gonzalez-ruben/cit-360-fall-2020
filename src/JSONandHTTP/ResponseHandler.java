package JSONandHTTP;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class ResponseHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        Person pers = new Person();
        pers.setName("Ruben");
        pers.setZipcode(11111);

        String json = JSON.personToJSON(pers);
        exchange.sendResponseHeaders(200, json.length());
        OutputStream output = exchange.getResponseBody();
        output.write(json.getBytes());
        output.close();
    }
}
