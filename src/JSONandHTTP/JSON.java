package JSONandHTTP;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSON {
    public static String personToJSON(Person person){
        ObjectMapper mapper = new ObjectMapper();
        String jsonstring = "";
        try {
            jsonstring = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return jsonstring;
    }
    public static Person JSONtoPerson(String jsonstring){
        ObjectMapper mapper = new ObjectMapper();
        Person person = null;
        try{
            person = mapper.readValue(jsonstring, Person.class);
        } catch (JsonProcessingException e){
            System.err.println(e.toString());
        }
        return person;
    }


}
